This script allows for Creeper World 3 to have turn based Vs multiplayer.
This repository also includes a demo map which shows off how the script is intended to be used.

This script was originally created by: thepenguin
Forum Post: https://knucklecracker.com/forums/index.php?topic=17899.msg126721#msg126721




======================================
How to use the script in your own map:
======================================

1. Make sure the "Master.crpl" script is in the "scripts" folder in your map's folder.
(it is under the "scripts" folder in this repository)
(it should look something like this: "creeperworld3/WorldEditor/YOURMAPNAME/scripts/Master.crpl")
2. In the world editor click the "unit" tab and then the "Scripts" button
3. Click "COMPILE ALL" (it should say something like: "# scripts Successfully compiled into #### opcodes.")
4. Add a CRPLCore unit
5. Double click to open the properties window
6. Make the Core invisible, invincible, and disable all creeper related check boxes.
7a.
	image -> none
	Take Map Space -> false
	create power zone -> false
	supports digitalis -> false
	counts for victory -> false
	nullifier damages -> false
8. Under "Attached Scripts", select "Master.crpl" and click "Add"
9. Click the pause button once to open the creeper shop menu
10. Save

11. (optional) copy over my example map's unit costs for balance
(under the "unit limits" button) 







===================
Editing the script:
===================

This allows you to change the game's balance or set a creeper win timer.

Open the "Master.crpl" file in either notepad or notepad++
(tutorial on how to set up notepad++ syntax highlighting here:
https://knucklecracker.com/wiki/doku.php?id=xrpl:creating_xrpl_scripts)

The variables you can mess with are between:

#======================================#
# start of balance and setup variables #
#======================================#

and

#====================================#
# end of balance and setup variables #
#====================================#


I commented what most of the variables do and the ones I didn't should be explanatory from their names.
Here are a few notable examples that you may want to change or just view as examples:

# this is how long in minutes the human player has to take their turn
# it will be rounded to the nearest whole number (you can't have 0.5)
1 ->PlayMinutes

# this is how much money the creeper player starts with
1500 ->CreeperBudget

# this is how many turns until the creeper player wins
# -2 means the game goes on forever
# any number greater than or equal to 1 is valid
-2 ->TotalNumberTurns


# this is how much a level 1 emitter costs
800 ->EmitterInitialCost

# this is used to determine how much more expensive an emitter will
# be to build per each level
1.15 ->EmitterUpgradeCostFactor


# this is how deep the creeper has to be to in order to place a new emitter
# NOTE: other creeper structures don't have this restriction and can be placed anywhere
2 ->CreepBuildMin